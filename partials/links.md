<a class="inventory" href="https://inventaire.io/inventory/max" title="Inventaire" target="_blank" ><i class="fa fa-book"></i><span>Inventory</span></a>
<a class="email" href="mailto:max<at>maxlath.eu?subject=Hi!" title="Email"><i class="fa fa-envelope"></i><span>Email</span></a>
<a class="pgp" href="/pgp.txt" title="PGP Key" target="_blank"><i class="fa fa-key"></i><span>PGP Key</span></a>
<a class="twitter" href="https://twitter.com/maxlath" title="Twitter" target="_blank" rel="me"><i class="fa fa-twitter"></i><span>Twitter</span></a>
<a class="wikidata" href="https://www.wikidata.org/wiki/Special:Contributions/Zorglub27" title="Wikidata" target="_blank"><i class="fa fa-barcode"></i><span>Wikidata</span></a>
<a class="wikipedia" href="https://fr.wikipedia.org/wiki/Sp%C3%A9cial:Contributions/Zorglub27" title="Wikipedia" target="_blank"><i class="fa fa-wikipedia-w"></i><span>Wikipedia</span></a>
<a class="commons" href="https://commons.wikimedia.org/w/index.php?title=Special:ListFiles/Zorglub27&ilshowall=1" title="Commons" target="_blank"><i class="fa fa-photo"></i><span>Commons</span></a>
<a class="openstreetmap" href="http://www.openstreetmap.org/user/maxlath" title="OpenStreetMap" target="_blank"><i class="fa fa-map-o"></i><span>Open Street Map</span></a>
<a class="gratipay" href="https://gratipay.com/~maxlath/" title="Gratipay" target="_blank" ><i class="fa fa-heart"></i><span>Gratipay</span></a>
<a class="github" href="http://github.com/maxlath/" title="Github" target="_blank"><i class="fa fa-github"></i><span>Github</span></a>
<a class="npm" href="https://www.npmjs.com/~maxlath" title="npm" target="_blank"><i class="fa fa-cubes"></i><span>NPM</span></a>
<a class="transifex" href="https://www.transifex.com/accounts/profile/maxlath" title="Transifex" target="_blank"><i class="fa fa-language"></i><span>Transifex</span></a>
<a class="framasphere" href="https://framasphere.org/people/e249fdb03e7e013341922a0000053625" title="Framasphere" target="_blank" rel="me"><i class="fa fa-asterisk"></i><span>Framasphere</span></a>
<a class="linkedin" href="http://www.linkedin.com/in/maximelathuiliere" title="LinkedIn" target="_blank" rel="me"><i class="fa fa-linkedin-square"></i><span>Linkedin</span></a>
<a class="stackoverflow" href="http://stackoverflow.com/users/3324977/maxlath" title="StackOverflow" target="_blank"><i class="fa fa-stack-overflow"></i><span>Stack Overflow</span></a>
