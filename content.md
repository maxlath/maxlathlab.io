<!DOCTYPE html>
<html>
<!-- HEAD --><!-- HEAD -->
<body>
  <main>
    <div id="about">
      <img id="pic" src="/assets/img/200-maxlath.jpg" />
      <a href="#about" name="#about"><h1>Maxime Lathuilière</h1></a>
      <!-- ABOUT --><!-- ABOUT -->
    </div>

    <!-- ITEMSLISTS --><!-- ITEMSLISTS -->
</main>
<footer>
  <div class="links"><!-- LINKS --><!-- LINKS --></div>
  <section id="tags"><!-- TAGS --><!-- TAGS --></section>
  <small>
    <a href="http://maxlath.eu/rss.xml" type="application/rss+xml"><i class="fa fa-rss"></i> RSS feed</a>
    &nbsp;-&nbsp;
    <a href="https://github.com/maxlath/maxlath.github.io" target="_blank">this website source code</a>
  </small>
</footer>
</body>
</html>